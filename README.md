# Cross-platform-app: Linux Client

This template/demo shows how to use a cross-platform business-logic codebase written in C++ in a GTK+-based Linux application.

The template therefore provides 3 different demos:

1. The basic usage of the [C++-library](https://gitlab.com/cross-platform-app/core-template).
2. The rendering of a [C++-based OpenGL (ES) implementation](https://gitlab.com/cross-platform-app/gl-example).
3. An example how to access a System-API, like the device-location via the [C++-library](https://gitlab.com/cross-platform-app/core-template).

_This example was only built and tested on Fedora 27. There is no warranty that it will build on any other distribution._

## Build Dependencies
- gtkmm-3.0 >= 3.22.2
- libgeoclue-2.0
- pkg-config >= 1.3.12
- doxygen >= 1.8.13 *(optional)*

**Important:** Make sure to also meet the dependencies in the submodules `Core` and `gl-example`

## Build Instructions

1. Initialize all submodules with `git submodule update --init --recursive`

2. Configure Makefile from CMake with `cmake -DCMAKE_INSTALL_PREFIX:PATH=/usr -H. -B_builds`

3. Go to `_builds` and run
    ```
    make
    make install
    ```
    
## Know Issues
### Geoclue
The access of the Geoclue-location-API does not work out of the box, since I have not found a way to ask for permission to use the API yet. This means that you need to enable access manually.
1. make sure the provided .desktop-file has been installed (running `make install` will do this for you).
2. Click the **Update Location**-button once. This will fail. You will get an error message like this in stderr:
    ```
    GDBus.Error:org.freedesktop.DBus.Error.AccessDenied: Agent rejected 'org.cross-platform-app.linux' for user '1000'. Please ensure that 'org.cross-platform-app.linux' has installed a valid org.cross-platform-app.linux.desktop file.

    ```
1. since you know you have installed a valid .desktop file, don't worry about this misleading error.
2. Launch `gnome-control-center`, if you are on Gnome or some equivalent Desktop.
3. Go to "Privacy" \> "Location Service" \> and enable the switch near the entry for "Linux template".
4. If you now request a location-update it should work.

## Documentation
generate documentation with target `Client--doc`. 

The target is only available if a matching version of doxygen could be found on the system (see requirements). 

The generated documentation can be found under `doc/html`

## Screenshots

![Screenshots of the Demo](screenshots.png)

## Troubleshooting

### Compiling

#### Linker error libstdc++.so.6 DSO missing
```commandline
/usr/bin/ld: CMakeFiles/Client.dir/src/MainWindow.cpp.o: undefined reference to symbol '_ZTVN10__cxxabiv121__vmi_class_type_infoE@@CXXABI_1.3'
//usr/lib64/libstdc++.so.6: error adding symbols: DSO missing from command line
collect2: Fehler: ld gab 1 als Ende-Status zurück
```
**Solution:**
try with another compiler, e.g. `g++`

