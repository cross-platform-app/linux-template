#include "NotificationRevealer.hpp"
#include <glibmm/main.h>

NotificationRevealer::NotificationRevealer(BaseObjectType *cobject, const Glib::RefPtr<Gtk::Builder> &builder)
        : Gtk::Revealer(cobject),
          _builder(builder)
{
    _builder->get_widget("buttonNotificationAction", _buttonNotificationAction);
    _builder->get_widget("buttonNotificationClose", _buttonNotificationClose);
    _builder->get_widget("labelNotificationMessage", _labelNotificationMessage);
    _buttonNotificationClose->signal_clicked().connect([this]{
        this->hideNotification();
    });
}

void NotificationRevealer::hideNotification() {
    this->set_reveal_child(false);
}

void NotificationRevealer::showNotification(unsigned int duration_ms) {
    int currentNotification = ++_notificationCounter;
    this->set_reveal_child(true);
    Glib::signal_timeout().connect_once([this, currentNotification]{
        // only hide the notification if it is the current notification and not a newer one.
        if(currentNotification == this->_notificationCounter) {
            this->hideNotification();
        }
    }, duration_ms);
}

void NotificationRevealer::setMessage(const std::string message) {
    _labelNotificationMessage->set_text(message);
}

void NotificationRevealer::setAction(const std::string action, std::function<void(void)> lambda) {
    _buttonNotificationAction->set_label(action);
    _buttonNotificationAction->set_visible(true);
    _buttonNotificationAction->signal_clicked().connect(lambda);
}

void NotificationRevealer::disableAction() {
    _buttonNotificationAction->set_visible(false);
}