#ifndef LINUX_TEMPLATE_MAINWINDOW_H
#define LINUX_TEMPLATE_MAINWINDOW_H


#include <gtkmm/window.h>
#include <gtkmm/applicationwindow.h>
#include <gtkmm/builder.h>
#include <gtkmm/box.h>
#include <gtkmm/label.h>
#include <gtkmm/button.h>
#include <gtkmm/scale.h>
#include <gtkmm/glarea.h>
#include <gtkmm/headerbar.h>
#include "GlExampleArea.hpp"
#include "CounterBox.hpp"
#include "GeolocationBox.hpp"
#include "OpenGLBox.hpp"
#include "NotificationRevealer.hpp"
#include "core/CounterInterface.hpp"
#include "GraphicsBox.hpp"

//! Main Program Window.
class MainWindow : public Gtk::ApplicationWindow {

protected:


public:
    /*!
     * Constructor
     * @param builder reference to builder, needed for loading layouts
     */
    MainWindow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder> builder);

    static MainWindow* create();

protected:
    Glib::RefPtr<Gtk::Builder> _builder;

    CounterBox* _counterBox;
    GeolocationBox* _geolocationBox;
    OpenGLBox* _openGLBox;
    GraphicsBox* _graphicsBox;
    NotificationRevealer* _notificationRevealer;

};


#endif //LINUX_TEMPLATE_MAINWINDOW_H
