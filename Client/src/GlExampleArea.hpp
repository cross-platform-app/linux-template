#pragma once

#include <gtkmm/glarea.h>
#include <gtkmm/builder.h>
#include "glExample/GlExampleInterface.hpp"
#include <GL/gl.h>

/// Custom Widget to provide the rendering of the OpenGL example.
/**
 * This overrides the default Gtk::GLArea to configure the context in the constructor and provide the custom calls
 * needed to display the Cube from the _gl-example_-submodule.
 *
 * **Disclaimer:** This implementation is not very efficient, since the rendering loop runs all the time, even if the
 * GLArea is not visible. I'm sure there is a way to pause the loop when the area gets hidden, but have not found it yet.
 */
class GlExampleArea : public Gtk::GLArea {
public:
    GlExampleArea(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& builder );

private:
    std::shared_ptr<glExample::GlExampleInterface> _glExample;
    bool _needsResize;

protected:
    bool on_render(const Glib::RefPtr<Gdk::GLContext>&context) override;
    void on_resize(int width, int height) override;
};