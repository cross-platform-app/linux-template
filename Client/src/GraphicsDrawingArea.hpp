#pragma once

#include "gtkmm/drawingarea.h"
#include "graphics/GraphicsInterface.hpp"
#include "LinuxCanvas.hpp"
#include "gtkmm/builder.h"

class GraphicsDrawingArea : public Gtk::DrawingArea {
public:
    GraphicsDrawingArea(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& builder );
    bool on_draw(const Cairo::RefPtr <Cairo::Context> &cr) override;

private:
    Cairo::RefPtr<Cairo::Context> _cr;
    std::shared_ptr<LinuxCanvas> _canvas = nullptr;
    std::shared_ptr<graphics::GraphicsInterface> _graphics = nullptr;
};