#pragma once

#include "graphics/CanvasInterface.hpp"
#include "graphics/Skalar.hpp"
#include "graphics/Paint.hpp"
#include "cairomm/context.h"


class LinuxCanvas: public graphics::CanvasInterface {
public:

    LinuxCanvas(const Cairo::RefPtr <Cairo::Context> &context);

    void setContext(const Cairo::RefPtr <Cairo::Context> &context);

    void save() override;

    void restore() override;

    void translate(const graphics::Skalar & skalar) override;

    void drawLines(const std::vector<graphics::Skalar> & points, const graphics::Paint & paint) override;

private:
    Cairo::RefPtr<Cairo::Context> _context;
};

