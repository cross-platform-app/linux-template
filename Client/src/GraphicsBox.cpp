#include "GraphicsBox.hpp"


GraphicsBox::GraphicsBox(BaseObjectType *cobject, const Glib::RefPtr<Gtk::Builder> &builder)
        : Gtk::Box(cobject),
          _builder(builder)
{
    _builder->get_widget_derived("drawingAreaGraphics", _graphicsDrawingArea);


}
