#pragma once

#include "gtkmm/box.h"
#include "gtkmm/builder.h"
#include "GraphicsDrawingArea.hpp"

class GraphicsBox : public Gtk::Box {
public:
    GraphicsBox(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& builder );
protected:
    Glib::RefPtr<Gtk::Builder> _builder;
    GraphicsDrawingArea * _graphicsDrawingArea;
};
