#pragma once

#include "core/LocationInterface.hpp"
#include "core/OnChangeListener.hpp"
#include "core/LocationRecord.hpp"
#include <libgeoclue-2.0/gclue-simple.h>

/// Implementation of the LocationInterace from the _core_-submodule.
/**
 * This exposes the Geoclue-API on Linux to be used in the cross-platform Logic-Tier.
 * This implementation is very simple, since it can just fetch the last known location.
 * More information on Geoclue can be found [here](https://gitlab.freedesktop.org/geoclue/geoclue/wikis/home).
 * This example needs a valid .desktop file for the application installed. For more information have a look
 * [here](https://wiki.ubuntuusers.de/.desktop-Dateien/).
 * The CMake configuration of this implementation provides an install-rule to install the .desktop file. Just make sure
 * to run `make install` at least once.
 * The .desktop file is used by the DBus to register the permission to access the location API.
 *
 * This implementation has one major issue: It can not ask for permission when the Geoclue-service is
 * requested for the first time, because I have not figured out how to do this yet.
 * To manually enable Geoclue access, run updateLocation() once. It will fail. Then (on Gnome) go to the system-settings
 * application and enable the location-access for the application manually.
 */
class LinuxLocation : public core::LocationInterface {
public:
    LinuxLocation();
    void updateLocation(const std::shared_ptr<core::OnChangeListener> & onChangeListener) override;

    core::LocationRecord getLocation() override;

private:
    GClueSimple* _geoclue = nullptr;

    std::shared_ptr<core::OnChangeListener> _onChangeListener;
    core::LocationRecord _location;
};
