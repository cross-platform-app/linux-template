#include "GlExampleArea.hpp"
#include <glibmm/main.h>


GlExampleArea::GlExampleArea(BaseObjectType *cobject, const Glib::RefPtr<Gtk::Builder> &builder) : Gtk::GLArea(cobject) {
    set_has_depth_buffer(true);
    set_double_buffered(true);
    set_required_version(3, 2);

    // start the rendering loop in the main-thread.
    // the passed lambda gets called every 20ms
    Glib::signal_timeout().connect([this]{
        queue_render();
        return true;
    }, 20, G_PRIORITY_LOW);

    _needsResize = true;
}

bool GlExampleArea::on_render(const Glib::RefPtr<Gdk::GLContext> &context) {
    // glExample needs to be initialized in here, because its constructor contains OpenGL-calls.
    if(_glExample == NULL) {
        _glExample = glExample::GlExampleInterface::GlExample();
        // set background-color to blend in with the Adwaita gtk-theme
        _glExample->setColor(.91, .91, .91);
    }
    if(_needsResize) {
        int scaleFactor = get_scale_factor(); // the scale-factor of the UI on high-resolution displays.
        _glExample->setViewport(get_width() * scaleFactor, get_height() * scaleFactor);
        _needsResize = false;
    }

    // call the actual rendering code
    _glExample->render();
    return true;
}

void GlExampleArea::on_resize(int width, int height) {
    _needsResize=true;
}