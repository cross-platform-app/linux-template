#pragma once

#include <gtkmm/box.h>
#include <gtkmm/builder.h>
#include <gtkmm/button.h>
#include <gtkmm/label.h>
#include <gtkmm/spinner.h>
#include "core/CounterInterface.hpp"
#include "NotificationRevealer.hpp"

/// Box for editing and displaying the counter-state
/**
 * This example shows how to interact with a C++-based library in an asynchronous matter.
 */
class CounterBox : public Gtk::Box {
public:
    CounterBox(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& builder );

    /// called when the user clicks the **Increase Counter**-button.
    /**
     * triggers the function in the Logic-Tier that increases the counter in a new thread.
     */
    void onButtonIncreaseCounterClicked();

    /// updates the Label that displays the current counter-state
    void updateCounterDisplay();

    /// sets the state of the ui-elements that can change the counter-state
    /**
     * @param canInteract The ui can have two different states:
     *                      - `true`:  the user can change the counter-state. The buttons **Increase Counter** and **Reset**
     *                                 are enabled.
     *                      - `false`: the user can not change the counter-state, since the last state change has not
     *                                 yet returned with a callback. The buttons are disabled and a loading spinner is
     *                                 displayed.
     */
    void setStateCounterCanInteract(bool canInteract);

    /// display an error message if something went wrong when increasing the counter.
    /**
     * @param show `true`: the error message is visible; `false`: the error message is hidden.
     */
    void showCounterError(bool show);

    /// called when the user clicks the **Reset**-button.
    /**
     * triggers the function in the Logic-Tier that resets the counter in a new thread.
     */
    void onButtonResetCounterClicked();
protected:
    Glib::RefPtr<Gtk::Builder> _builder;

    Gtk::Button* _buttonIncreaseCounter;
    Gtk::Button* _buttonResetCounter;
    Gtk::Label* _labelCounter;
    Gtk::Spinner* _spinnerLoadCounter;

    NotificationRevealer* _notificationRevealer;


    std::shared_ptr<core::CounterInterface> _counter;
};
