#pragma once

#include <gtkmm/revealer.h>
#include <gtkmm/button.h>
#include <gtkmm/label.h>
#include <gtkmm/builder.h>


class NotificationRevealer: public Gtk::Revealer {
public:
    NotificationRevealer(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& builder );

    void showNotification(unsigned int duration_ms);
    void hideNotification();
    void setMessage(const std::string message);
    void setAction(const std::string action, std::function<void(void)> lambda);
    void disableAction();
private:
    Glib::RefPtr<Gtk::Builder> _builder;
    Gtk::Button* _buttonNotificationAction;
    Gtk::Button* _buttonNotificationClose;
    Gtk::Label* _labelNotificationMessage;
    int _notificationCounter;



};


