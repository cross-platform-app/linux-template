#pragma once

#include <gtkmm/box.h>
#include <gtkmm/builder.h>
#include "GlExampleArea.hpp"

/// Box that holds the GLArea that renders the OpenGL-example from _gl-example_-submodule.
class OpenGLBox : public Gtk::Box {
public:
    OpenGLBox(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& builder );

protected:
    Glib::RefPtr<Gtk::Builder> _builder;

    GlExampleArea* _glarea;
};
