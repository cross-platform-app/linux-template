#include "GeolocationBox.hpp"
#include "LinuxLocation.hpp"
#include "core/LocationRecord.hpp"
#include "core/OnChangeListener.hpp"
#include <thread>
#include <glibmm/main.h>

/// OnChangeListener that updates GeolocationBox when the counter-state changed.
class OnLocationChangedListener: public core::OnChangeListener {
public:

    /**
     * @param geolocationBox instance of GeolocationBox that should be updated.
     */
    explicit OnLocationChangedListener(GeolocationBox* geolocationBox) {
        _geolocationBox = geolocationBox;
    }

    void onSuccess() override {
        Glib::signal_idle().connect_once([this]{
            _geolocationBox->setStateUpdateLocationLoading(false);
            _geolocationBox->showLocationError(false);
            _geolocationBox->updateLocationDisplay();
        }, G_PRIORITY_DEFAULT_IDLE);
    }

    void onError() override {
        Glib::signal_idle().connect_once([this]{
            _geolocationBox->setStateUpdateLocationLoading(false);
            _geolocationBox->showLocationError(true);
        }, G_PRIORITY_DEFAULT_IDLE);
    }

private:
    GeolocationBox* _geolocationBox;
};

GeolocationBox::GeolocationBox(BaseObjectType *cobject, const Glib::RefPtr<Gtk::Builder> &builder)
        : Gtk::Box(cobject),
          _builder(builder)
{
    // get the references to the ui elements defined in XML
    _builder->get_widget("buttonUpdateLocation", _buttonUpdateLocation);
    _builder->get_widget("labelLatitude", _labelLatitude);
    _builder->get_widget("labelLongitude", _labelLongitude);
    _builder->get_widget("spinnerLoadLocation", _spinnerLoadLocation);
    _builder->get_widget_derived("revealerNotification", _notificationRevealer);

    // initialize locationService instance from the Logic-Tier (Core-submodule) and register the OnChangeListener for GeolocationBox
    _locationService = core::LocationServiceInterface::LocationService(std::shared_ptr<LinuxLocation>(new LinuxLocation()));
    _locationService->onLocationChanged(std::shared_ptr<core::OnChangeListener>(new OnLocationChangedListener(this)));

    // connect button signal
    _buttonUpdateLocation->signal_clicked().connect([this]{
        onButtonUpdateLocationClicked();
    });

    // set initial UI-state.
    // sets the state of UI elements whose initial state might not be defined correctly in the XML
    setStateUpdateLocationLoading(false);
    showLocationError(false);
}

void GeolocationBox::updateLocationDisplay() {
    auto location = _locationService->getLocation();
    _labelLatitude->set_text(std::to_string(location.lat));
    _labelLongitude->set_text(std::to_string(location.lon));
}

void GeolocationBox::onButtonUpdateLocationClicked() {
    setStateUpdateLocationLoading(true);
    auto thread = std::thread([this]{
        _locationService->updateLocation();
    });
    thread.detach();
}

void GeolocationBox::showLocationError(bool show) {
    if(show) {
        _notificationRevealer->setMessage("Error updating location!");
        _notificationRevealer->disableAction();
        _notificationRevealer->showNotification(2000);
    } else {
        _notificationRevealer->hideNotification();
    }
}

void GeolocationBox::setStateUpdateLocationLoading(bool loading) {
    if(loading) {
        _spinnerLoadLocation->start();
        _buttonUpdateLocation->set_sensitive(false);
    } else {
        _spinnerLoadLocation->stop();
        _buttonUpdateLocation->set_sensitive(true);
    }
}