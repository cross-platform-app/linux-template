#pragma once

#include <gtkmm/box.h>
#include <gtkmm/builder.h>
#include <gtkmm/label.h>
#include <gtkmm/button.h>
#include <gtkmm/spinner.h>
#include "core/LocationServiceInterface.hpp"
#include "NotificationRevealer.hpp"

/// Box that demonstrates how to consume a system-API like the geolocation through a wrapper provided by the Logic-Tier.
class GeolocationBox : public Gtk::Box {
public:
    GeolocationBox(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& builder );

    /// updates the labels that display the location data (Latitude & Longitude)
    void updateLocationDisplay();

    /// sets the state of the UI-elements related to location-fetching
    /**
     * @param loading The UI can have two states:
     *                  - `true`:  the user can not interact with the **Update Location**-button because the last update
     *                             request has not returned yet. A loading spinner is displayed, to give the user the
     *                             advice to wait.
     *                  - `false`: the user can request a location-update. The button is clickable and the loading spinner
     *                             is hidden.
     */
    void setStateUpdateLocationLoading(bool loading);

    /// called when the **Update Location**-button is clicked.
    /**
     * triggers the function in the Logic-Tier that updates the location in a new thread.
     */
    void onButtonUpdateLocationClicked();

    /// displays an error message indicating that something went wrong while updating the location.
    /**
     * @param show `true`: the error message is visible; `false`: the error message is hidden.
     */
    void showLocationError(bool show);
private:
    Glib::RefPtr<Gtk::Builder> _builder;

    Gtk::Button* _buttonUpdateLocation;
    Gtk::Spinner* _spinnerLoadLocation;
    Gtk::Label* _labelLatitude;
    Gtk::Label* _labelLongitude;

    NotificationRevealer * _notificationRevealer;

    std::shared_ptr<core::LocationServiceInterface> _locationService;
};

