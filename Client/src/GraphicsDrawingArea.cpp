#include "GraphicsDrawingArea.hpp"
#include <iostream>

GraphicsDrawingArea::GraphicsDrawingArea(BaseObjectType *cobject, const Glib::RefPtr<Gtk::Builder> &builder)
        : Gtk::DrawingArea(cobject) {}

bool GraphicsDrawingArea::on_draw(const ::Cairo::RefPtr<::Cairo::Context> &cr) {
    if (_graphics == nullptr) {
        _cr = cr;
        _canvas = std::make_shared<LinuxCanvas>(cr);
        _graphics = graphics::GraphicsInterface::Graphics(_canvas);
    }
    if(cr != _cr) {
        _cr = cr;
        _canvas->setContext(_cr);
    }
    _graphics->draw();
}
