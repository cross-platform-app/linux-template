#include "MainWindow.h"
#include "config.h"
#include "core/OnChangeListener.hpp"
#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glext.h>
#include <iostream>


MainWindow::MainWindow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder> builder)
        : Gtk::ApplicationWindow(cobject),
          _builder(builder)
{
    _builder->get_widget_derived("revealerNotification", _notificationRevealer);
    _builder->get_widget_derived("boxCounter", _counterBox);
    _builder->get_widget_derived("boxGeolocation", _geolocationBox);
    _builder->get_widget_derived("boxOpenGL", _openGLBox);
    _builder->get_widget_derived("boxGraphics", _graphicsBox);
}

MainWindow* MainWindow::create() {
    // Load the Builder file and instantiate its widgets.
    auto refBuilder = Gtk::Builder::create_from_resource(GRESOURCES_PREFIX "ui/counter.glade");

    MainWindow* mainWindow = nullptr;
    refBuilder->get_widget_derived("mainWindow", mainWindow);
    return mainWindow;

}
