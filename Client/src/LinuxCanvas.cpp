#include "LinuxCanvas.hpp"

LinuxCanvas::LinuxCanvas(const Cairo::RefPtr<Cairo::Context> &context) {
    setContext(context);
}

void LinuxCanvas::setContext(const Cairo::RefPtr<Cairo::Context> &context) {
    _context = context;
}

void LinuxCanvas::save() {
    _context->save();
}

void LinuxCanvas::restore() {
    _context->restore();
}

void LinuxCanvas::translate(const graphics::Skalar &skalar) {
    _context->translate(skalar.x, skalar.y);
}

void LinuxCanvas::drawLines(const std::vector<graphics::Skalar> &points, const graphics::Paint &paint) {
    _context->move_to(points[0].x, points[0].y);
    for (int i = 1; i < points.size(); i++) {
        _context->line_to(points[i].x, points[i].y);
    }

    _context->set_line_width(paint.lineWidth);

    switch(paint.style) {
        case graphics::Style::FILL:
            _context->set_source_rgba(paint.fillColor.red, paint.fillColor.green, paint.fillColor.blue, paint.fillColor.alpha);
            _context->fill();
            break;
        case graphics::Style::STROKE:
            _context->set_source_rgba(paint.strokeColor.red, paint.strokeColor.green, paint.strokeColor.blue, paint.strokeColor.alpha);
            _context->stroke();
            break;
        case graphics::Style::FILLANDSTROKE:
            _context->set_source_rgba(paint.fillColor.red, paint.fillColor.green, paint.fillColor.blue, paint.fillColor.alpha);
            _context->fill_preserve();
            _context->set_source_rgba(paint.strokeColor.red, paint.strokeColor.green, paint.strokeColor.blue, paint.strokeColor.alpha);
            _context->stroke();
            break;
    }

}
