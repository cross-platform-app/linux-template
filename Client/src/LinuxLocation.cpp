
#include "LinuxLocation.hpp"
#include "config.h"
#include <libgeoclue-2.0/gclue-location.h>
#include <iostream>

LinuxLocation::LinuxLocation() : _location(0,0) {
}

void LinuxLocation::updateLocation(const std::shared_ptr<core::OnChangeListener> &onChangeListener) {
    _onChangeListener = onChangeListener;
    // create geoclue-object if we first call this function
    if(_geoclue == nullptr) {
        GError* gerror = nullptr;
        _geoclue = gclue_simple_new_sync(APPLICATION_ID, GCLUE_ACCURACY_LEVEL_EXACT, nullptr, &gerror);
        if(gerror != nullptr) {
            std::cerr << gerror->message << std::endl;
            _onChangeListener->onError();
            return;
        }
    }
    auto location = gclue_simple_get_location(_geoclue);
    _location.lat = gclue_location_get_latitude(location);
    _location.lon = gclue_location_get_longitude(location);
    _onChangeListener->onSuccess();
}

core::LocationRecord LinuxLocation::getLocation() {
    return _location;
}