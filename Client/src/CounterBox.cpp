#include "CounterBox.hpp"
#include "core/OnChangeListener.hpp"
#include <thread>
#include <glibmm/main.h>

/// OnChangeListener that updates CounterBox when the counter-state changed.
class OnNumberChangedListener : public core::OnChangeListener {
public:

    /**
     * @param counterBox instance of CounterBox that should be updated.
     */
    explicit OnNumberChangedListener(CounterBox* counterBox) {
        _counterBox = counterBox;
    }

    void onSuccess() override {
        Glib::signal_idle().connect_once([this]{
            _counterBox->setStateCounterCanInteract(true);
            _counterBox->showCounterError(false);
            _counterBox->updateCounterDisplay();
        }, G_PRIORITY_DEFAULT_IDLE);
    }

    void onError() override {
        Glib::signal_idle().connect_once([this]{
            _counterBox->setStateCounterCanInteract(true);
            _counterBox->showCounterError(true);
        }, G_PRIORITY_DEFAULT_IDLE);
    }
private:
    CounterBox* _counterBox;
};

CounterBox::CounterBox(BaseObjectType *cobject, const Glib::RefPtr<Gtk::Builder> &builder)
        : Gtk::Box(cobject),
          _builder(builder)
{
    // get the references to the ui elements defined in XML
    _builder->get_widget("buttonIncreaseCounter", _buttonIncreaseCounter);
    _builder->get_widget("buttonResetCounter", _buttonResetCounter);
    _builder->get_widget("labelCounter", _labelCounter);
    _builder->get_widget("spinnerLoadCounter", _spinnerLoadCounter);
    _builder->get_widget_derived("revealerNotification", _notificationRevealer);

    // initialize counter instance from the Logic-Tier (Core-submodule) and register the OnChangeListener for CounterBox
    _counter = core::CounterInterface::Counter();
    _counter->onNumberChanged(std::shared_ptr<core::OnChangeListener>(new OnNumberChangedListener(this)));

    // connect button signals
    _buttonIncreaseCounter->signal_clicked().connect([this]{
       this->onButtonIncreaseCounterClicked();
    });
    _buttonResetCounter->signal_clicked().connect([this]{
        this->onButtonResetCounterClicked();
    });

    // set initial ui state.
    // sets the state of UI-elements whose initial state might not be defined correctly in the XML
    setStateCounterCanInteract(true);
    showCounterError(false);
}

void CounterBox::onButtonIncreaseCounterClicked() {
    setStateCounterCanInteract(false);
    // call Logic-Tier action in a new thread to keep UI responsive if calculation takes a little longer
    auto thread = std::thread([this]{
        _counter->increaseNumber();
    });
    thread.detach();
}

void CounterBox::onButtonResetCounterClicked() {
    setStateCounterCanInteract(false);
    // call Logic-Tier action in a new thread to keep UI responsive if calculation takes a little longer
    auto thread = std::thread([this]{
        _counter->resetNumber();
    });
    thread.detach();
}

void CounterBox::setStateCounterCanInteract(bool canInteract) {
    if(canInteract) {
        _spinnerLoadCounter->stop();
        _buttonIncreaseCounter->set_sensitive(true);
        _buttonResetCounter->set_sensitive(true);
    } else {
        _spinnerLoadCounter->start();
        _buttonIncreaseCounter->set_sensitive(false);
        _buttonResetCounter->set_sensitive(false);
    }
}

void CounterBox::showCounterError(bool show) {
    if(show) {
        _notificationRevealer->setMessage("Error increasing the Counter!");
        _notificationRevealer->setAction("Reset", [this]{
            this->onButtonResetCounterClicked();
            this->showCounterError(false);
        });
        _notificationRevealer->showNotification(2000);
    } else {
        _notificationRevealer->hideNotification();
    }
}

void CounterBox::updateCounterDisplay() {
    _labelCounter->set_text(std::to_string(_counter->getNumber()));
}