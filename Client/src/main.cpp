#include <gtkmm/main.h>
#include "MainWindow.h"
#include "config.h"
#include RESOURCE_FILE


int main(int argc, char *argv[]) {
    // initialize Application environment
    const auto app = Gtk::Application::create( argc, argv, APPLICATION_ID );

    auto mainWindow = MainWindow::create();

    // initialize resources
    resource_constructor();
    // start program loop
    return app->run(*mainWindow, argc, argv);
}
